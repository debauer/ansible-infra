#!/usr/bin/python
from __future__ import annotations

from ansible.module_utils.basic import AnsibleModule

COMMANDS = []


def remove_command(fqdn):
    global COMMANDS
    COMMANDS.append(f"/ip dns static remove [find name={fqdn}]")


def add_command(fqdn, ip, ttl):
    global COMMANDS
    COMMANDS.append(f"/ip dns static add name={fqdn} ttl={ttl} address={ip}")


def filter_hosts(hosts, hostvars):
    host_list = []
    for host in hosts:
        if "ip" in hostvars[host]:
            host_list.append(host)
    return host_list


def parse_dns_export(dns_static):
    data = {}
    for x in dns_static:
        if x.startswith("add"):
            fqdn = x.split("name=")[1].split(" ")[0]
            ip = x.split("address=")[1].split(" ")[0]
            data[fqdn] = ip
    return data


def new_dns_records(host_list, domain, hostvars):
    data = {}
    for host in host_list:
        data[f"{host}.{domain}"] = hostvars[host]["ip"]
        if "fqdn" in hostvars[host]:
            for fqdn in hostvars[host]["fqdn"]:
                data[fqdn] = hostvars[host]["ip"]
    return data


def main():
    module_args = dict(
        hostvars=dict(type="dict", required=True),
        hosts=dict(type="list", required=True),
        dns_static=dict(type="list", required=True),
    )
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    hostvars = module.params["hostvars"]
    hosts = module.params["hosts"]
    ttl = hostvars['mikrotik']['dns']['ttl']
    domain = hostvars['mikrotik']['dns']['domain']
    filtered_hosts = filter_hosts(hosts, hostvars)
    dns_export = parse_dns_export(module.params['dns_static'][0])
    dns_new = new_dns_records(filtered_hosts, domain, hostvars)

    for fqdn, ip in dns_export.items():
        if fqdn not in dns_new.keys():
            remove_command(fqdn)
        else:
            if ip != dns_new[fqdn]:
                remove_command(fqdn)
                add_command(fqdn, dns_new[fqdn], ttl)
    for fqdn, ip in dns_new.items():
        if fqdn not in dns_export.keys():
            add_command(fqdn, ip, ttl)

    module.exit_json(
        changed=True, commands=COMMANDS
    )


if __name__ == "__main__":
    main()
