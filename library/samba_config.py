#!/usr/bin/python
from __future__ import annotations

from ansible.module_utils.basic import AnsibleModule


def _wrap(key, name, config):
    if key in config:
        return f"{name} = {config[key]}\n"
    return ""


def build_conf_part(config):
    ret = f"[{config['name']}]\n"
    ret += f"path = {config['path']}\n"
    ret += _wrap("comment", "comment", config)
    ret += _wrap("guest_ok", "guest ok", config)
    ret += _wrap("read_only", "read only", config)
    ret += _wrap("create_mode", "create mode", config)
    ret += _wrap("dir_mode", "directory mode", config)
    if "user_config" in config:
        ret += f"force user = {config['user_config']}\n"
        ret += f"force group = {config['user_config']}\n"
    ret += f"\n"
    return ret


def main():
    global HOSTVARS, TASMOTAVARS
    module_args = dict(
        samba_configs=dict(type="list", required=True),
    )
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    configs = module.params["samba_configs"]
    main_configs = [x for x in configs if "user_config" not in x]

    main_config = ""
    for mc in main_configs:
        main_config += build_conf_part(mc)

    usernames = {x["user_config"] for x in configs if "user_config" in x}
    user_configs = []
    for user in usernames:
        confs = [x for x in configs if "user_config" in x and x["user_config"] == user]
        user_config = ""
        for mc in confs:
            user_config += build_conf_part(mc)
        user_configs.append([user, user_config])

    module.exit_json(
        changed=True, main=main_config, user_configs=user_configs,
    )


if __name__ == "__main__":
    main()
