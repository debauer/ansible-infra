#!/usr/bin/python
from __future__ import annotations

import threading

from ansible.module_utils.basic import AnsibleModule

import subprocess  # For executing a shell command

REACHABLE = []


def ping(target: str, host: str) -> None:
    global REACHABLE

    command = ['ping', '-c', '1', target, '-W', '3']

    if subprocess.call(command) == 0:
        REACHABLE.append(host)


def main() -> None:
    module_args = dict(
        hosts=dict(type="list", required=True),
        hostvars=dict(type="dict", required=True),
    )
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    hosts = module.params["hosts"]
    hostvars = module.params["hostvars"]
    threads = []
    for host in hosts:
        target = hostvars[host]["ip"] if "ip" in hostvars[host] else hostvars[host]["ansible_host"]
        threads.append(threading.Thread(target=ping, args=(target,host)))

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    module.exit_json(
        changed=True, reachable=REACHABLE
    )


if __name__ == "__main__":
    main()
