#!/usr/bin/python
from __future__ import annotations

from copy import copy

from ansible.module_utils.basic import AnsibleModule


COMMANDS = []
HOSTVARS = {}
TASMOTAVARS = {}


def add_command_by_var_name(cmd, var_name, default=None, restart=False):
    if var_name in TASMOTAVARS:
        add_command(cmd, TASMOTAVARS[var_name], restart)
    elif var_name.startswith("tasmota") and var_name in HOSTVARS:
        add_command(cmd, HOSTVARS[var_name], restart)
    else:
        if default is None:
            return
        add_command(cmd, default, restart)


def add_command(cmd, value, restart=False):
    global COMMANDS
    COMMANDS.append(
        {
            "command": cmd,
            "value": value,
            "restart": restart,
        },
    )


def main():
    global HOSTVARS, TASMOTAVARS
    module_args = dict(
        hostvars=dict(type="dict", required=True),
        hostname=dict(type="str", required=True),
        vault=dict(type="dict", required=True),
    )
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    hostname = module.params["hostname"]
    vault = module.params["vault"]
    HOSTVARS = module.params["hostvars"][hostname]
    if "tasmota" in HOSTVARS:
        TASMOTAVARS = HOSTVARS["tasmota"]
        add_command_by_var_name("TelePeriod", "tele_period")
        add_command_by_var_name("FriendlyName1", "friendly_name", hostname)
        add_command_by_var_name("DeviceName", "friendly_name", hostname)
        add_command_by_var_name("Timers", "timers_active")
        add_command("Hostname", hostname)
        add_command("LedState", 0)
        add_command("SetOption21", 1)
        add_command("SetOption53", 1)

        if "timer" in TASMOTAVARS:
            timer_is_active = sum([x != 0 for x in TASMOTAVARS["timer"]])
            for i, t in enumerate(TASMOTAVARS["timer"]):
                if t != 0:
                    timer_is_active = True
                    add_command(f"Timer{i + 1}", t)
            add_command("Timers", 1 if timer_is_active else 0)
        wlan = vault["tasmota"]["wlan"]
        add_command("Ssid1", wlan["ssid1"], restart=True)
        add_command("Ssid2", wlan["ssid2"], restart=True)
        add_command("Password1", wlan["password1"], restart=True)
        add_command("Password2", wlan["password2"], restart=True)
        if "module" in TASMOTAVARS:
            add_command_by_var_name("Module", "module", restart=True)

        if "dhcp" in HOSTVARS:
            add_command("IPAddress1", "0.0.0.0", restart=True)
            add_command("IPAddress2", "0.0.0.0", restart=True)
            add_command("IPAddress3", "0.0.0.0", restart=True)
            add_command("IPAddress4", "0.0.0.0", restart=True)
        else:
            add_command_by_var_name("IPAddress1", "ip", restart=True)
            add_command_by_var_name("IPAddress2", "gateway", restart=True)
            add_command_by_var_name("IPAddress3", "network", restart=True)
            add_command_by_var_name("IPAddress4", "dns", restart=True)

        add_command_by_var_name("FullTopic", "tasmota_mqtt_fulltopic", restart=True)
        add_command_by_var_name("MqttHost", "tasmota_mqtt_host", restart=True)
        add_command_by_var_name("MqttPassword", "tasmota_mqtt_password", restart=True)
        add_command_by_var_name("MqttUser", "tasmota_mqtt_user", restart=True)
        add_command_by_var_name("MqttPort", "tasmota_mqtt_port", restart=True)
        add_command_by_var_name("MqttClient", "friendly_name", default=hostname, restart=True)
        add_command_by_var_name("Topic", "tasmota_mqtt_topic", restart=True)

    backlog = ""
    for c in copy(COMMANDS):
        if c["restart"]:
            backlog += f"{c['command']} {c['value']};"
            COMMANDS.remove(c)
    for x in COMMANDS:
        del x["restart"]

    if backlog != "":
        COMMANDS.append(
            {
                "command": "backlog",
                "value": backlog,
            },
        )

    module.exit_json(
        changed=True, commands=COMMANDS, backlog=backlog,
    )


if __name__ == "__main__":
    main()
