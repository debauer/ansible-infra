#!/usr/bin/python
from __future__ import annotations

from ansible.module_utils.basic import AnsibleModule

COMMANDS = []


def remove_command(mac):
    global COMMANDS
    COMMANDS.append(f"/ip dhcp-server lease remove [find mac-address={mac}]")


def add_command(mac, ip, dhcp_name):
    global COMMANDS
    COMMANDS.append(f"/ip dhcp-server lease add mac-address={mac} address={ip} server={dhcp_name} use-src-mac=yes")


def filter_hosts(hosts, hostvars):
    host_list = []
    for host in hosts:
        if "ip" in hostvars[host] and "mac" in hostvars[host]:
            host_list.append(host)
    return host_list


def parse_dhcp_export(dhcp_static):
    data = {}
    for x in dhcp_static:
        if x.startswith("add"):
            ip = x.split("address=")[1].split(" ")[0]
            mac = x.split("mac-address=")[1].split(" ")[0]
            data[mac.upper()] = ip
    return data


def new_dhcp_records(host_list, hostvars):
    data = {}
    for host in host_list:
        data[hostvars[host]["mac"].upper()] = hostvars[host]["ip"]
    return data


def main():
    module_args = dict(
        hostvars=dict(type="dict", required=True),
        hosts=dict(type="list", required=True),
        dhcp_lease=dict(type="list", required=True),
    )
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    hostvars = module.params["hostvars"]
    hosts = module.params["hosts"]

    dhcp_name = hostvars['mikrotik']['dhcp']['name']
    filtered_hosts = filter_hosts(hosts, hostvars)
    dhcp_export = parse_dhcp_export(module.params['dhcp_lease'][0])
    dhcp_new = new_dhcp_records(filtered_hosts, hostvars)

    for mac, ip in dhcp_export.items():
        if mac not in dhcp_new.keys():
            remove_command(mac)
        else:
            if ip != dhcp_new[mac]:
                remove_command(mac)
                add_command(mac, dhcp_new[mac], dhcp_name)
    for mac, ip in dhcp_new.items():
        if mac not in dhcp_export.keys():
            add_command(mac, ip, dhcp_name)

    module.exit_json(
        changed=True, f1=filtered_hosts, commands=COMMANDS, new=dhcp_new
    )


if __name__ == "__main__":
    main()
